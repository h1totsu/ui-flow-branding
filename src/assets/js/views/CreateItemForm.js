import Marionette from 'backbone.marionette';
import createItemTemplate from '../templates/createItemForm.hbs';

const CreateItemForm = Marionette.View.extend({
    initialize() {
        this.template = createItemTemplate;
    }
})

export default CreateItemForm;