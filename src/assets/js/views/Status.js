import Marionette from 'backbone.marionette';
import Backbone from 'backbone';
import statusTemplate from '../templates/status.hbs';
import TaskList from './TaskList';

const Status = Marionette.View.extend({
    tagName: 'div',
    className: 'column column-block',

    regions: {
        tasks: {
            el: '.task-list',
        }
    },

    initialize(options) {
        this.template = statusTemplate;
        this.tasks = options.tasks;
    },

    serializeData() {
        return {
            displayName: this.model.get('displayName')
        };
    },

    onRender() {
        this.getRegion('tasks').show(new TaskList({
            collection: this.filterByStatusId(this.model.get('id'))
        }));
    },

    filterByStatusId(statId) {
        let filteredCollection = this.tasks.where({statusId: statId});
        return new Backbone.Collection(filteredCollection);
    }
})

export default Status;