import Marionette from 'backbone.marionette';
import userTemplate from '../templates/user.hbs';

const User = Marionette.View.extend({
    tagName: 'li',

    initialize() {
        this.template = userTemplate;
    }
})

export default User;