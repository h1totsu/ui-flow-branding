import Marionette from 'backbone.marionette';
import taskTemplate from '../templates/task.hbs';

const Task = Marionette.View.extend({
    tagName: 'div',
    className: 'card task',

    initialize() {
        this.template = taskTemplate;
    },

    serializeData() {
        return {
            title: this.model.get('title'),
            description: this.model.get('description')
        };
    },

})

export default Task;