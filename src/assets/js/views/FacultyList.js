import Marionette from 'backbone.marionette';
import facultiesTemplate from '../templates/faculties.hbs';
import Faculties  from '../models/collections/Faculties';

const FacultyList = Marionette.View.extend({
    initialize() {
        this.template = facultiesTemplate;
    },

    serializeData() {
        let faculties = new Faculties();
        faculties.fetch({async: false});
        
        return {
            faculties: faculties.models.map(faculty => {
                return faculty.attributes;
            })
        };
    },
})

export default FacultyList;