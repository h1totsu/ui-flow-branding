import Marionette from 'backbone.marionette';
import Task from '../views/Task';

const TaskList = Marionette.CollectionView.extend({
    childView: Task
})

export default TaskList;