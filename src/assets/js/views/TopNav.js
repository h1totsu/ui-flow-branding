import Marionette from 'backbone.marionette';
import topNavTemplate from '../templates/topNav.hbs';

const TopNav = Marionette.View.extend({
    tagName: 'div',
    className: 'top-bar-left',
    
    initialize() {
        this.template = topNavTemplate;
    }
})

export default TopNav;