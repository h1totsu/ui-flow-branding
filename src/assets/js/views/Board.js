import Marionette from 'backbone.marionette';
import Statuses from '../models/collections/Statuses';
import Status from '../views/Status';
import Tasks from '../models/collections/Tasks';

const Board = Marionette.CollectionView.extend({
    tagName: 'div',
    className: 'row expanded medium-up-5 small-up-1',

    childView: Status,

    childViewOptions() {
        return {
            tasks: this.tasks
        }
    },

    initialize(options) {
        this.collection = new Statuses();
        this.collection.fetch({async: false});

        this.tasks = new Tasks();
        this.tasks.fetch({async: false});
    },
})

export default Board;