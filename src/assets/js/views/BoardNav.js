import Marionette from 'backbone.marionette';
import navTemplate from '../templates/boardNav.hbs';

const BoardNav = Marionette.View.extend({
    initialize() {
        this.template = navTemplate;
    }
})

export default BoardNav;