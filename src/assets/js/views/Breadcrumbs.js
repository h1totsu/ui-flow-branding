import Marionette from 'backbone.marionette';
import breadcrumbsTemplate from '../templates/breadcrumbs.hbs';

const Breadcrumbs = Marionette.View.extend({
    initialize() {
        this.template = breadcrumbsTemplate;
    }
})

export default Breadcrumbs;