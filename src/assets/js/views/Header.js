import Marionette from 'backbone.marionette';
import headerTemplate from '../templates/header.hbs';
import TopNav from '../views/TopNav';
import User from '../views/User';

const Header = Marionette.View.extend({
    regions: {
        topNav: {
            el: '#top-nav',
            view: new TopNav()
        },
        user: {
            el: '#user',
            view: new User()
        },
    },
    
    initialize() {
        this.template = headerTemplate;
        this.options.onBoard = false;
    },

    serializeData() {
        return {
            onBoard: this.getOption('onBoard'),
        };
    },

    onRender() {
        $.each(this.getRegions(), (key, region) => {
            region.show(region.options.view);
        });
    },
})

export default Header;