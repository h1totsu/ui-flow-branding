import Marionette from 'backbone.marionette';
import departmentsTemplate from '../templates/departments.hbs';
import Departments  from '../models/collections/Departments';

const DepartmentList = Marionette.View.extend({
    initialize(option) {
        this.facultyId = option.facultyId;
        this.template = departmentsTemplate;
    },

    serializeData() {
        let departments = new Departments({facultyId: this.facultyId});
        departments.fetch({async: false});

        return {
            departments: departments.models.map(department => {
                return department.attributes;
            })
        };
    },
})

export default DepartmentList;