import Marionette from 'backbone.marionette';
import footerTemplate from '../templates/footer.hbs';

const Footer = Marionette.View.extend({
    initialize() {
        this.template = footerTemplate;
    }
})

export default Footer;