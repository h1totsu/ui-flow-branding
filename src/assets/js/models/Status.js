import Backbone from 'backbone';
import {BASE_URL} from '../constants/settings';

const Status = Backbone.Model.extend({
    idAttribute : 'id',
    urlRoot: BASE_URL + 'taskStatuses',
});

export default Status;