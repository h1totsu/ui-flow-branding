import Backbone from 'backbone';
import {BASE_URL, FACULTY} from '../../constants/settings';

const Faculties = Backbone.Collection.extend({
    url: BASE_URL + 'groups/search/findByType?type=' + FACULTY,

    parse: function (data) {
        if (data._embedded) {
            return data._embedded.groups;
        } else {
            return data;
        }
    }
});

export default Faculties;