import Backbone from 'backbone';
import {BASE_URL} from '../../constants/settings';

const Departments = Backbone.Collection.extend({
    url() {
        return BASE_URL + 'groups/search/findSubGroups?parentId=' + this.facultyId;
    },

    initialize(options) {
        this.facultyId = options.facultyId;
    },

    parse: function (data) {
        if (data._embedded) {
            return data._embedded.groups;
        } else {
            return data;
        }
    }
});

export default Departments;