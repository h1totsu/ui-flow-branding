import Backbone from 'backbone';
import {BASE_URL} from '../../constants/settings';
import Task from '../Task';

const Tasks = Backbone.Collection.extend({
    model: Task, 
    url: BASE_URL + 'tasks',

    parse: function (data) {
        if (data._embedded) {
            return data._embedded.tasks;
        } else {
            return data;
        }
    }
});

export default Tasks;