import Backbone from 'backbone';
import {BASE_URL} from '../../constants/settings';
import Status from '../Status';

const Statuses = Backbone.Collection.extend({
    model: Status,
    url: BASE_URL + 'taskStatuses',

    parse: function (data) {
        if (data._embedded) {
            return data._embedded.taskStatuses;
        } else {
            return data;
        }
    }
});

export default Statuses;