import Backbone from 'backbone';
import {BASE_URL} from '../constants/settings';

const Task = Backbone.Model.extend({
    idAttribute : 'id',
    urlRoot: BASE_URL + 'tasks'
});

export default Task;