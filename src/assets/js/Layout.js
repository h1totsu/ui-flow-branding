import Marionette from 'backbone.marionette';
import layoutTemplate from './templates/layout.hbs';
import Header from './views/Header';
import Footer from './views/Footer';
import Board from './views/Board';
import FacultyList from './views/FacultyList';
import DepartmentList from './views/DepartmentList';
import CreateItemForm from './views/CreateItemForm';

const Layout = Marionette.View.extend({
    initialize() {
        this.template = layoutTemplate;
    },

    regions: {
        header: {
            el: '.site-header',
        },
        footer: {
            el: '.site-footer',
        },
        boardNav: {
            el: '.project-nav',
        },
        breadcrumbs: {
            el: '.app-breadcrumbs',
        },
        board: {
            el: '.board',
        }
    },

    renderRegions() {
        this.getRegion('header').show(new Header());
        this.getRegion('footer').show(new Footer());
        this.getRegion('board').show(new FacultyList());
    },

    onShowBoard() {
        this.getRegion('board').show(new Board());
    },

    onShowFaculties() {
        this.getRegion('board').show(new FacultyList());
    },

    onShowDepartments(facultyId) {
        this.getRegion('board').show(new DepartmentList({facultyId: facultyId}));
    },

    onShowCreateForm() {
        this.getRegion('board').show(new CreateItemForm());
    }
});

export default Layout;