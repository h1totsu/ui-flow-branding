import $ from 'jquery';
import 'foundation';
import Marionette from 'backbone.marionette';
import Backbone from 'backbone';
import Layout from './Layout';
import Router from './router/Router';

let App = new Marionette.Application();

App.on('start', () => {
    App.rootLayout = new Layout({el: '.project-main'});
    App.rootLayout.render();
    App.rootLayout.renderRegions();

    new Router({layout: App.rootLayout});
    
    if (Backbone.history) {
        Backbone.history.start();
    }

    $(document).foundation();

    $('button[data-toggle="project-navigation"]').on('click', ()=> {
        $('.secondary-navigation').toggleClass('opened');
        $('.main-content').toggleClass('navigation-opened');
    });
    // $('.project-main .column').matchHeight();
});

App.start();

