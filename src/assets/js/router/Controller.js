import Marionette from 'backbone.marionette';

const Controller = Marionette.Object.extend({
    initialize(options) {
        this.options.layout = options.layout;
    },

    showBoard() {
        this.getOption('layout').triggerMethod('show:board');
    },

    showFaculties() {
        this.getOption('layout').triggerMethod('show:faculties');
    },

    showDepartments(facultyId) {
        this.getOption('layout')
            .triggerMethod('show:departments', facultyId);
    },

    showCreateForm() {
        this.getOption('layout')
            .triggerMethod('show:create:form');
    }
});

export default Controller;