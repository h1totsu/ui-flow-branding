import Marionette from 'backbone.marionette';
import Controller from './Controller';

const Router = Marionette.AppRouter.extend({
    appRoutes: {
        '/': 'showFaculties',
        'board': 'showBoard',
        'faculty': 'showFaculties',
        'department/:id': 'showDepartments',
        'create': 'showCreateForm'
    },

    initialize(options) {
        this.controller = new Controller({layout: options.layout});
    },
    
    onRoute(route) {
        console.error('triggered ' + route)
    }
});

export default Router;